// Read in and parse JSON file
const inputFile = require('./input.json')
const inputArr = JSON.parse(inputFile).map(x => {
  return(x.input)
})

// PART 1

const countSteps = (input, regex, negative = false) => {
  let res = inputArr
  .filter((x) => x.match(regex))
  .map((x) => +x.match(/\d+/g))
  .reduce((a, b) => a + b, 0)

  if(negative){
    return -res
  }

  return res
}

let fwd = countSteps(inputArr, /forward/g)
let down = countSteps(inputArr, /down/g)
let up = countSteps(inputArr, /up/g, true)

console.log(fwd * (down + up))

// PART 2

const calculateAim = (input) => {

  let out = input.map((x) => {
    if (/down/g.test(x)) {
      return +x.match(/\d+/g)
    } else if (/up/g.test(x)) {
      return -x.match(/\d+/g)
    } else {
      return 0
    }
  })
  return calculateCumulativeSum(out)
}

const calculateDepth = (input, aim) => {
  let depth = []
  Object.keys(input).forEach((i) => {
    if (/forward/g.test(input[i])) {
      depth.push(aim[i] * +input[i].match(/\d+/g))
    } else {
      depth.push(0)
    }
  })
  return depth.reduce((a, b) => a + b, 0)
}

const calculateCumulativeSum = (input) => {
  let cumSumArr = []
  input.reduce((prev, curr, i) => cumSumArr[i] = prev + curr, 0)
  return cumSumArr
}

let aim = calculateAim(inputArr)
let depth = calculateDepth(inputArr, aim)

console.log(fwd * depth)
