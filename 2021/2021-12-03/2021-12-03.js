// Read in and parse JSON file
const inputFile = require('./input.json')
const inputArr = JSON.parse(inputFile).map(x => {
  return(x.input)
})

let binaryLength = inputArr[0].length

// PART 1 ----------------------------------------------------------------------

// Useful functions

const createSequence = (n) => {
  let arr = []
  for(let i = 0; i < n; i++){
    arr.push(i+1)
  }
  return arr
}

const findOptimum = (array, which = 'gamma') => {
  let unique = [...new Set(array)]
  let optimalIndex
  let out = unique.map((x) => {
    return array.filter((y) => {
      return y === x
    }).length
  })

  if (which === 'gamma') {
    optimalIndex = out.indexOf(Math.max(...out))
  } else if (which === 'epsilon') {
    optimalIndex = out.indexOf(Math.min(...out))
  }

  return unique[optimalIndex]
}

const getRate = (array, which = "gamma") => {
  let arr = createSequence(binaryLength)
  .map((i) => {
    return array.map((x) => {
      return x[i-1]
    })
  })
  .map((x) => findOptimum(x, which))
  .reduce((a, b) => a.concat(b), '')

  return parseInt(arr, 2)
}

let rate = ['gamma', 'epsilon'].map((x) => {
  return getRate(inputArr, x)
})

console.log(rate.reduce((a, b) => a * b))


// PART 2 ----------------------------------------------------------------------

const allEqual = (arr) => {
  return arr.every(v => v === arr[0])
}

const findOptimum2 = (array, which) => {
  let unique = [...new Set(array)]
  let optimalIndex
  let out = unique.map((x) => {
    return array.filter((y) => {
      return y === x
    }).length
  })
  if (which === 'oxygen') {
    if(allEqual(out)) return 1
    else optimalIndex = out.indexOf(Math.max(...out))
  } else if (which === 'co2') {
    if(allEqual(out)) return 0
    else optimalIndex = out.indexOf(Math.min(...out))
  }
  return unique[optimalIndex]
}

const getRating = (array, which = 'oxygen') => {
  let arr = createSequence(binaryLength)
  .map((i) => {
    return array.map((x) => {
      return x[i-1]
    })
  })
  let binary = arr.map((x) => findOptimum2(x, which)).reduce((a, b) => a.concat(b), '')
  return parseInt(binary, 2)
}

let rating = ['oxygen', 'co2'].map((x) => {
  return getRating(inputArr, x)
})

console.log(rating.reduce((a, b) => a * b));
