// Shoutout to: https://www.youtube.com/watch?v=-ihdC-AKqPM

// Read in and parse JSON file
const inputFile = require('./input.json')
const input = JSON.parse(inputFile).map(x => {
  return(x.input)
})

// console.log(input);

const getFishNumber = (input, days) => {
  const fishCount = Array(9).fill(0)
  for (const fish of input) {
    fishCount[fish]++
  }
  for (let i = 0; i < days; i++) {
    const fish0 = fishCount.shift()
    fishCount.push(fish0)
    fishCount[6] += fish0
  }
  console.log(fishCount.reduce((a, b) => a + b, 0))
}

getFishNumber(input, 80)
getFishNumber(input, 256)
