// Read in and parse JSON file
const inputFile = require('./input.json')
const inputArr = JSON.parse(inputFile).map(x => {
  return(x.input)
})

// Custom functions

const splitArray = (arr, chunkSize) => {
  let out = []
  for(let i = 0; i < arr.length; i += chunkSize){
    out.push(arr.slice(i, i+chunkSize))
  }
  return out
}

const findPosition = (index) => {
  index++
  let row = (Math.floor((index - 1) / 5)) + 1
  return {row: row-1, col: (index - (5 * (row - 1)))-1}
}

const countIndex = (array) => {
  let indices = ['row', 'col'].map((dim) => {
    return array.map((x) =>  x[dim])
  })
  return indices.map((x) => {
    let unique = [...new Set(x)]
    return unique.map((i) => {
      let l = x.filter((y) => y === i).length
      return {[i]: l}
    })
  })
}

// Clean up inputs: draws and boards

const draws = inputArr[0]
.split(',')
.map((x) => Number(x))
const boards0 = inputArr
boards0.shift()
const boards1 = boards0
.filter((x) => x != '')
.map((x) => {
  return x
  .replace(/^\s/, '')
  .replace(/\s{2}/g, ' ')
})
const boards = splitArray(boards1, 5)
.map((x) => {
  return x
  .map((y) => y.split(' '))
  .flat()
  .map((z) => Number(z))
})


// Match draws to boards

let positionMatchesArr = draws.map((drawValue, drawKey) => {
  let out = boards.map((board) => {
    let position
    let index = board.indexOf(drawValue)
    if(index === -1) position = {row: 99, col: 99}
    else position = findPosition(index)
    return {i: drawKey, draw: drawValue, index: index,  ...position}
  })
  return out
})

let rowMatchesCount = positionMatchesArr.map((drawData) => {
  return drawData.reduce((accumulator, element, index) => {
    if(accumulator.length === 0) {
      accumulator.push(element)
    } else {
      let prev = accumulator[index-1]
      let newObj = {}
      Object.keys(element).forEach((key) => {
        newObj[key] = prev[key] + element[key]
      })
      accumulator.push(newObj)
    }
    return accumulator
  }, [])
})

console.log(positionMatchesArr);
