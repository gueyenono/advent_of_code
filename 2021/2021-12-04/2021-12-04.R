# Load package(s) ----

library(dplyr)
library(furrr)

# Get puzzle input ----

input <- httr::GET(
  "https://adventofcode.com/2021/day/4/input", 
  httr::set_cookies(session = Sys.getenv("aoc_cookie"))
) |>
  httr::content(as = "text") |>
  strsplit(split = "\n") |>
  unlist()

# # -----------------------------------------------
# input <- httr::GET(
#   "https://adventofcode.com/2021/day/4",
#   httr::set_cookies(session = Sys.getenv("aoc_cookie"))
# )
# 
# code <- input |>
#   httr::content(as = "text") |>
#   rvest::read_html() %>%
#   rvest::html_element(css = "code") %>%
#   rvest::html_text()
# 
# draws <- strsplit(x = code, split = "\\n{2}")[[1]][1] %>%
#   strsplit(split = ",") %>%
#   purrr::pluck(1) %>%
#   as.numeric()
# 
# boards <- strsplit(x = code, split = "\\n{2}")[[1]][-1] %>%
#   stringr::str_replace_all(pattern = "\\n", replacement = " ") %>%
#   stringr::str_squish() %>%
#   strsplit(split = " ") %>%
#   purrr::map(as.numeric)
# 
# # ----------------------------------------------------------------------

# Write puzzle input to disk ----

jsonlite::toJSON(data.frame(input = input)) |>
  jsonlite::write_json(path = here::here("2021/2021-12-04/input.json"))

# Custom function(s) ----

find_position <- function(index){
  row <- ((index - 1) %/% 5) + 1
  col <- index - (5 * (row - 1))
  data.frame(row, col)
}

# Clean input ----

draws <- strsplit(input[1], split = ",")[[1]] %>% as.numeric()
fct <- cumsum(input[-(1:2)] == "")
boards <- split(x = trimws(input[-(1:2)]), f = fct) %>%
  purrr::map(function(board_data){
    if(length(board_data) == 6) d <- board_data[-1]
    else d <- board_data
    strsplit(paste0(d, collapse = " "), split = "\\s+")[[1]] %>% as.numeric()
  })
names(boards) <- NULL

# PREP WORK ----

# Determine positive matches in each board sequentially (0 means no match)
positions_list <- purrr::map(boards, function(board){
    purrr::map_dfr(draws, function(x){
      i <- which(board == x)
      pos <- find_position(i)
      
      if(nrow(pos) == 0){
        pos <- tibble::tibble(row = 0, col = 0)
      } 
      
      tibble(draw = x) %>%
        bind_cols(pos)
    }) %>%
    mutate(i = row_number()) %>%
    relocate(i, 1) %>%
    tidyr::pivot_longer(cols = -c(i, draw), names_to = "dim", values_to = "index")
}) %>%
  purrr::map(function(pos){
    pos %>%
      group_by(dim, index) %>%
      mutate(count = row_number()) %>%
      ungroup() 
  })

# Determine the condition of completion of each board
completion_list <- positions_list %>%
  purrr::map(function(x){
    x %>%
      filter(index != 0, count == 5) %>%
      slice_min(order_by = i, n = 1)
  })


# PART 1 ----


n_draws_list <- purrr::map_dbl(completion_list, ~ unique(.x$i))
index <- which(n_draws_list == min(n_draws_list)) # Completion index
n_draws <- n_draws_list[index] # The board was completed after {n_draws} draws
first_completed_board <- completion_list[[index]]

realized_draws <- draws[seq_len(n_draws)]
unmatched_draws <- setdiff(boards[[index]], realized_draws)
last_draw <- completion_list[[index]]$draw

sum(unmatched_draws) * last_draw


# PART 2 ----

index2 <- which(n_draws_list == max(n_draws_list)) # Completion index
n_draws2 <- n_draws_list[index2] # The board was completed after {n_draws} draws
last_completed_board <- completion_list[[index2]]

realized_draws2 <- draws[seq_len(n_draws2)]
unmatched_draws2 <- setdiff(boards[[index2]], realized_draws2)
last_draw2 <- unique(completion_list[[index2]]$draw)

sum(unmatched_draws2) * last_draw2
