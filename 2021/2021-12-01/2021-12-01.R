# Get puzzle input

input <- httr::GET(
  "https://adventofcode.com/2021/day/1/input", 
  httr::set_cookies(session = Sys.getenv("aoc_cookie"))
) |>
  httr::content(as = "text") |>
  strsplit(split = "\n") |>
  unlist() |>
  as.numeric()

# Write puzzle input to disk

jsonlite::toJSON(data.frame(input = input)) |>
  jsonlite::write_json(path = here::here("2021/2021-12-01/input.json"))

# Part 1

sum(diff(input) > 0)


# Part 2

input2 <- zoo::rollapply(input, width = 3, by = 1, FUN = sum)
sum(diff(input2) > 0)
