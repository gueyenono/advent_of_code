// Check https://attacomsian.com/blog/nodejs-read-write-json-files for
// methods of importing json data

// Read in and parse JSON file
const inputFile = require('./input.json')
const inputArr = JSON.parse(inputFile[0]).map(x => {
  return(x.input)
})

// PART 1

let diffArr = []

// Check if

for(let i = 1; i <= inputArr.length-1; i++){
  diffArr.push(inputArr[i] - inputArr[i-1])
}

let results = diffArr.filter(x => {
  return(x > 0)
}).length

console.log(results);


// PART 2

let rollSumArr = []

for(i = 0; i <= inputArr.length-2; i++){

  let arrayOf3 = [i, i+1, i+2].map((i) => {
    return(inputArr[i])
  })

  let sum = arrayOf3.reduce((a, b) => a + b, 0)

  rollSumArr.push(sum)
}

let diffArr2 = []

for(let i = 1; i <= rollSumArr.length-1; i++){
  diffArr2.push(rollSumArr[i] - rollSumArr[i-1])
}


let results2 = diffArr2.filter(x => {
  return(x > 0)
}).length

console.log(results2);
