# PART 1 ----

# > Input ----

input1_test <- readLines(con = here::here("input/day2_part1_test_input.txt"))
input1 <- readLines(con = here::here("input/day2_part1_input.txt"))
# input <- input1_test

# > Function ----

part1 <- function(input, content = c("red" = 12, "green" = 13, "blue" = 14)){
  purrr::imap_dfr(.x = strsplit(x = input, split = "; "), .f = \(game_record, game_number){
    purrr::map_dfr(.x = game_record, .f = \(set){
      purrr::map2_dfr(.x = names(content), .y = content, .f = \(color, number){
        out <- stringr::str_extract_all(string = set, pattern = glue::glue("\\d+\\s{color}")) |> unlist() |>
          stringr::str_extract(string = _, pattern = "\\d+") |> as.numeric()
        n_out <- ifelse(length(out) > 0, out, 0)
        data.frame(game = game_number, possible = n_out <= number)
      })
    })
  }) |>
    dplyr::summarize(possible = all(possible), .by = c(game)) |>
    dplyr::filter(possible) |>
    dplyr::pull(game) |> as.numeric() |> sum()
}

part1(input = input1_test)
part1(input = input1)


# PART 2 ----

# > Input ----

test2 <- readLines(con = here::here("input/day2_part2_test.txt"))
input2 <- readLines(con = here::here("input/day2_part2_input.txt"))

# > Function ----

part2 <- function(input, cube_colors = c("red", "green", "blue")){
  purrr::imap_dfr(.x = strsplit(x = input, split = "; "), .f = \(game_record, game_number){
    purrr::map_dfr(.x = game_record, .f = \(set){
      purrr::map_dfr(.x = cube_colors, .f = \(color){
        out <- stringr::str_extract_all(string = set, pattern = glue::glue("\\d+\\s{color}")) |> unlist() |>
          stringr::str_extract(string = _, pattern = "\\d+") |> as.numeric()
        n_out <- ifelse(length(out) > 0, out, 0)
        data.frame(game = game_number, color = color, n = n_out)
      })
    })
  }) |>
    dplyr::summarize(max_n = max(n), .by = c(game, color)) |>
    dplyr::summarize(power = prod(max_n), .by = c(game)) |>
    dplyr::pull(power) |> sum()
}

part2(input = test2)
part2(input = input2)
